import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class FlightsService {

  private flightsUri ='http://localhost:8080/flyaway/webapi/flights';

  constructor(private http:HttpClient, private router:Router) { }

  addAirport(flights){
    return this.http.post(this.flightsUri,flights)    
  }

  updateFlight(flights){
    return this.http.put(this.flightsUri,flights)
  }

deleteFlight(id){
    return this.http.get(this.flightsUri+"/"+id);
}

getAllFlights(){
    return this.http.get(this.flightsUri)
}

}
